# Análisis elemental de acordes

## Propósito

El objetivo del proyecto es exponer sistemáticamente la creación de un
programa para el análisis elemental de acordes según la teoría tradicional
de la música utilizando Python como medio de expresión.

El documento principal es `chord_analysis.pdf`, o bien `chord_analysis.ipynb`.
Véase la introducción de ese documento para una explicación de los
principios rectores, las decisiones relativas al diseño y otros aspectos
relevantes para la comprensión y el objetivo del proyecto.

## Audiencia

El documento mentado está dirigido a todos aquellos con mínimos conocimientos
de música e interés por acercarse a los problemas relacionados con su campo
a través del pensamiento abstracto y computacional. No se necesitan
conocimientos previos de programación, pero sí son un requisito si se quiere
entender en detalle el código resultante. Una comprensión a grandes rasgos
del código es posible ---espero--- sin tales conocimientos.

## Contenido de este directorio (orden alfabético)

`chord_analysis.ipynb`:
  ~ Jupyter Notebook con el texto principal

`chord_analysis.md`:
  ~ Fichero principal en formato markdown

`chord_analysis.mscz`:
  ~ Fichero MuseScore con las ilustraciones musicales del texto principal

`chord_analysis.pdf`:
  ~ Fichero principal en formato pdf

`chord_analysis.py`:
  ~ Código fuente del texto principal

`customize_code.tex`:
  ~ Código TeX para el formateo del código fuente en pdf

`extract_code_blocks.sh`:
  ~ Guión para la extracción de bloques de código en `chord_analysis.py`

`img/`:
  ~ Directorio con las imágenes insertados en el texto principal

## ¿Cómo generar alguno de los ficheros anteriores?

Los ficheros fundamentales son:

- `chord_analysis.md`
- `chord_analysis.py`

El código fuente está contenido en `chord_analysis.py`. El documento central,
`chord_analysis.md` intercala ese código con el texto que lo explica y comenta.
Las ilustraciones musicales fueron editadas con MuseScore y recortes de ella se
guardan en `img/`.

Se puede extraer el código fuente desde el propio fichero markdown, mediante

    ./extract_code_blocks.sh chord_analysis.md

El resultado contendrá algunos fragmentos que no aparecen en el código
fuente. En concreto, los _stubs_ de las funciones auxiliares.

El pdf se obtiene mediante [Pandoc](https://pandoc.org):

    pandoc -s -H customize_code.tex \
    --highlight-style 'tango' \
    chord_analysis.md -o chord_analysis.pdf

El _notebook_ de Jupyter se puede obtener también con Pandoc, aunque hay que
editar el resultado desde Jupyter para convertir los fragmentos de código
Python en celdas ejecutables.

## Requisitos

* Obligatorios

    - `Python >= 3.10`
    - `bidict`
    - `ordered-set`
    - `pytest`

* Opcionales (Jupyter _notebook_)

    - `jupyterlab`
    - `ipytest`

* Opcionales (_type checking_ y _linting_)

    - `mypy`
    - `pylint`

Con la excepción de Python, el resto de los requisitos se puede cumplir
instalando el correspondiente paquete con `pip`:

    pip install <nombre-del-paquete>

Por ejemplo, para instalar `bidict`:

    pip install bidict

## Uso 

- Ejecución de `chord_analysis.py`. Muestra el resultado del análisis de la
  pieza musical `SOR_35_9`, definida en `chord_analysis.py`:

        python chord_analysis.py

- Ejecución en modo interactivo. En este modo puede ejecutar las funciones
  definidas en `chord_analysis.py` de forma interactiva:

        python -i chord_analysis.py

- Ejecución de los tests:

        pytest chord_analysis.py

- Verificación de tipos:

        mypy chord_analysis.py

- _linting_:

        pylint --no-docstring-rgx '.*' chord_analysis.py

    `--no-docstring-rgx` elimina las advertencias de pylint sobre falta
    de documentación de las funciones. Véase el texto principal para una
    explicación de la ausencia de _docstrings_.

