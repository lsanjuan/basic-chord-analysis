"""
Prototype for traditional music chord analysis
"""

from collections import namedtuple
from itertools import permutations
from typing import (
    Any, List, Literal, Mapping, Optional, Sequence, Tuple,
    cast
)

from ordered_set import OrderedSet
from bidict import bidict

import pytest

# Note
Letter = Literal['A', 'B', 'C', 'D', 'E', 'F', 'G']
Accidental = Literal[
    'double_flat', 'flat', 'natural',
    'sharp', 'double_sharp'
]
Note = namedtuple('Note', ['letter', 'accidental'])
LetterNumber = Literal[0, 1, 2, 3, 4, 5, 6]
Pitchclass = Literal[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

# Interval
Interval = namedtuple('Interval', ['low', 'high'])
IntervalNumber = Literal[1, 2, 3, 4, 5, 6, 7]
IntervalQuality = Literal[
    'double_diminished', 'diminished', 'minor', 'perfect',
    'major', 'augmented', 'double_augmented'
]
IntervalAnalysis = namedtuple('IntervalAnalysis', ['number', 'quality'])
IntervalPattern = Tuple[Optional[IntervalAnalysis], ...]

# Chord
Chord = OrderedSet[Note]
ChordInversion = Literal[0, 1, 2, 3, 4, 5, 6]
ChordCategory = str
ChordAnalysis = Mapping[str, Any]

# Music symbols
ACCIDENTAL_SYMBOLS: bidict[Accidental, str] = bidict({
    'double_flat': 'bb',
    'flat': 'b',
    'natural': '',
    'sharp': '#',
    'double_sharp': 'x'
})

CHORD_CATEGORY_SYMBOLS: Mapping[ChordCategory, str] = {
    'major': '',
    'minor': '-',
    'augmented': '+',
    'diminished': '-b5',
    'half_diminished': '-7b5',
    'dominant_seventh': '7',
    'major_seventh': 'maj7',
    'minor_seventh': '-7',
    'diminished_seventh': 'o',
    'suspended_fourth': 'sus4',
    'suspended_second': 'sus2',
}

INTERVAL_QUALITY_SYMBOLS: bidict[IntervalQuality, str] = bidict({
    'double_diminished': 'dd',
    'diminished': 'd',
    'minor': 'm',
    'perfect': 'P',
    'major': 'M',
    'augmented': 'A',
    'double_augmented': 'AA'
})

# Accidental distance in semitones wrt. natural
ACCIDENTAL_OFFSETS: Mapping[Accidental, int] = {
    'double_flat': -2,
    'flat': -1,
    'natural': 0,
    'sharp': 1,
    'double_sharp': 2
}

# Interval qualities by interval number and semitones
INTERVAL_QUALITIES: Mapping[
    IntervalNumber,
    Mapping[int, IntervalQuality]] = {
    1: {-2: 'double_diminished',
        -1: 'diminished',
        0: 'perfect',
        1: 'augmented',
        2: 'double_augmented'
    },
    2: {-1: 'double_diminished',
        0: 'diminished',
        1: 'minor',
        2: 'major',
        3: 'augmented',
        4: 'double_augmented'
    },
    3: {1: 'double_diminished',
        2: 'diminished',
        3: 'minor',
        4: 'major',
        5: 'augmented',
        6: 'double_augmented'
    },
    4: {3: 'double_diminished',
        4: 'diminished',
        5: 'perfect',
        6: 'augmented',
        7: 'double_augmented'
    },
    5: {5: 'double_diminished',
        6: 'diminished',
        7: 'perfect',
        8: 'augmented',
        9: 'double_augmented'
    },
    6: {6: 'double_diminished',
        7: 'diminished',
        8: 'minor',
        9: 'major',
        10: 'augmented',
        11: 'double_augmented'
    },
    7: {8: 'double_diminished',
        9: 'diminished',
        10: 'minor',
        11: 'major',
        12: 'augmented',
        13: 'double_augmented'
    }
}

# Chord Interval Patterns and Categories
MAJOR = (
    IntervalAnalysis(3, 'major'),
    IntervalAnalysis(5, 'perfect')
)
MAJOR_NO5TH = (
    IntervalAnalysis(3, 'major'),
)
MINOR = (
    IntervalAnalysis(3, 'minor'),
    IntervalAnalysis(5, 'perfect')
)
MINOR_NO5TH = (
    IntervalAnalysis(3, 'minor'),
)
AUGMENTED = (
    IntervalAnalysis(3, 'major'),
    IntervalAnalysis(5, 'augmented')
)
DIMINISHED = (
    IntervalAnalysis(3, 'minor'),
    IntervalAnalysis(5, 'diminished')
)
HALF_DIMINISHED = (
    IntervalAnalysis(3, 'minor'),
    IntervalAnalysis(5, 'diminished'),
    IntervalAnalysis(7, 'minor')
)
DOMINANT_SEVENTH = (
    IntervalAnalysis(3, 'major'),
    IntervalAnalysis(5, 'perfect'),
    IntervalAnalysis(7, 'minor')
)
DOMINANT_SEVENTH_NO5TH = (
    IntervalAnalysis(3, 'major'),
    IntervalAnalysis(7, 'minor')
)
MAJOR_SEVENTH = (
    IntervalAnalysis(3, 'major'),
    IntervalAnalysis(5, 'perfect'),
    IntervalAnalysis(7, 'major')
)
MAJOR_SEVENTH_NO5TH = (
    IntervalAnalysis(3, 'major'),
    IntervalAnalysis(7, 'major')
)
MINOR_SEVENTH = (
    IntervalAnalysis(3, 'minor'),
    IntervalAnalysis(5, 'perfect'),
    IntervalAnalysis(7, 'minor')
)
MINOR_SEVENTH_NO5TH = (
    IntervalAnalysis(3, 'minor'),
    IntervalAnalysis(7, 'minor')
)
DIMINISHED_SEVENTH = (
    IntervalAnalysis(3, 'minor'),
    IntervalAnalysis(5, 'diminished'),
    IntervalAnalysis(7, 'diminished')
)
SUSPENDED_FOURTH = (
    IntervalAnalysis(4, 'perfect'),
    IntervalAnalysis(5, 'perfect')
)
SUSPENDED_SECOND = (
    IntervalAnalysis(2, 'major'),
    IntervalAnalysis(5, 'perfect')
)

CHORD_CATEGORIES: Mapping[IntervalPattern, str] = {
    MAJOR: 'major',
    MAJOR_NO5TH: 'major',
    MINOR: 'minor',
    MINOR_NO5TH: 'minor',
    AUGMENTED: 'augmented',
    DIMINISHED: 'diminished',
    HALF_DIMINISHED: 'half_diminished',
    DOMINANT_SEVENTH: 'dominant_seventh',
    DOMINANT_SEVENTH_NO5TH: 'dominant_seventh',
    MAJOR_SEVENTH: 'major_seventh',
    MAJOR_SEVENTH_NO5TH: 'major_seventh',
    MINOR_SEVENTH: 'minor_seventh',
    MINOR_SEVENTH_NO5TH: 'minor_seventh',
    DIMINISHED_SEVENTH: 'diminished_seventh',
    SUSPENDED_FOURTH: 'suspended_fourth',
    SUSPENDED_SECOND: 'suspended_second'
}

# Utils
def test_note_from_string():
    assert note_from_string('A') == Note('A', 'natural')
    assert note_from_string('D#') == Note('D', 'sharp')
    assert note_from_string('Eb') == Note('E', 'flat')
    with pytest.raises(ValueError):
        note_from_string('M')
        note_from_string('A3')

def note_from_string(note_str: str) -> Note:
    letter = letter_from_string(note_str[0])
    accidental = accidental_from_string(note_str[1:])
    return Note(letter, accidental)

N = note_from_string # abbrev.

def test_letter_from_string():
    assert letter_from_string('A') == 'A'
    assert letter_from_string('B') == 'B'
    assert letter_from_string('C') == 'C'
    assert letter_from_string('D') == 'D'
    assert letter_from_string('E') == 'E'
    assert letter_from_string('F') == 'F'
    assert letter_from_string('G') == 'G'
    with pytest.raises(ValueError):
        letter_from_string('H')

def letter_from_string(letter_str: str) -> Letter:
    match letter_str:
        case 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G':
            return letter_str
        case _:
            raise ValueError('Invalid note letter')

def test_accidental_from_string():
    assert accidental_from_string('bb') == 'double_flat'
    assert accidental_from_string('b') == 'flat'
    assert accidental_from_string('') == 'natural'
    assert accidental_from_string('#') == 'sharp'
    assert accidental_from_string('x') == 'double_sharp'
    with pytest.raises(ValueError):
        accidental_from_string('3')

def accidental_from_string(accidental_str: str) -> Accidental:
    try:
        accidental = ACCIDENTAL_SYMBOLS.inverse[accidental_str]
    except KeyError as exc:
        raise ValueError('Invalid accidental') from exc
    return accidental

def test_chord_from_strings():
    assert (
        chord_from_strings('C', 'E')
        == Chord([N('C'), N('E')])
    )
    assert (
        chord_from_strings('G', 'B', 'F')
        == Chord([N('G'), N('B'), N('F')])
    )

def chord_from_strings(*note_strs: str) -> Chord:
    notes = [note_from_string(n) for n in note_strs]
    return Chord(notes)

C = chord_from_strings # abbrev.

def test_interval_from_strings():
    assert interval_from_strings('A', 'C') == Interval(N('A'), N('C'))
    assert interval_from_strings('C', 'A') == Interval(N('C'), N('A'))

def interval_from_strings(note_str1: str, note_str2: str) -> Interval:
    note1 = note_from_string(note_str1)
    note2 = note_from_string(note_str2)
    return Interval(note1, note2)

I = interval_from_strings # abbrev.

def test_interval_analysis_from_string():
    assert (
        interval_analysis_from_string('P1')
        == IntervalAnalysis(1, 'perfect')
    )
    assert (
        interval_analysis_from_string('M2')
        == IntervalAnalysis(2, 'major')
    )
    assert (
        interval_analysis_from_string('d5')
        == IntervalAnalysis(5, 'diminished')
    )
    assert (
        interval_analysis_from_string('A4')
        == IntervalAnalysis(4, 'augmented')
    )
    assert (
        interval_analysis_from_string('AA6')
        == IntervalAnalysis(6, 'double_augmented')
    )
    assert (
        interval_analysis_from_string('dd7')
        == IntervalAnalysis(7, 'double_diminished')
    )
    with pytest.raises(ValueError):
        interval_analysis_from_string('P9')
        interval_analysis_from_string('x1')

def interval_analysis_from_string(
    interval_analysis_str: str
) -> IntervalAnalysis:
    number = interval_number_from_string(interval_analysis_str[-1])
    quality = interval_quality_from_string(interval_analysis_str[:-1])
    return IntervalAnalysis(number, quality)

def test_interval_number_from_string():
    assert interval_number_from_string('1') == 1
    assert interval_number_from_string('2') == 2
    assert interval_number_from_string('3') == 3
    assert interval_number_from_string('4') == 4
    assert interval_number_from_string('5') == 5
    assert interval_number_from_string('6') == 6
    assert interval_number_from_string('7') == 7
    with pytest.raises(ValueError):
        interval_number_from_string('0')

def interval_number_from_string(
    interval_number_str: str
) -> IntervalNumber:
    number = int(interval_number_str)
    if number < 1 or number > 7:
        raise ValueError('Invalid interval number')
    return cast(IntervalNumber, number)

def test_interval_quality_from_string():
    assert interval_quality_from_string('dd') == 'double_diminished'
    assert interval_quality_from_string('d') == 'diminished'
    assert interval_quality_from_string('m') == 'minor'
    assert interval_quality_from_string('P') == 'perfect'
    assert interval_quality_from_string('M') == 'major'
    assert interval_quality_from_string('A') == 'augmented'
    assert interval_quality_from_string('AA') == 'double_augmented'
    with pytest.raises(ValueError):
        interval_quality_from_string('x')

def interval_quality_from_string(
    interval_quality_str: str
) -> IntervalQuality:
    try:
        quality = INTERVAL_QUALITY_SYMBOLS.inverse[interval_quality_str]
    except KeyError as exc:
        raise ValueError('Invalid interval quality') from exc
    return quality

IA = interval_analysis_from_string # abbrev.

def test_interval_pattern_from_strings():
    assert (
        interval_pattern_from_strings('M3', 'P5')
        == (IA('M3'), IA('P5'))
    )
    assert (
        interval_pattern_from_strings('m3', 'd5', 'm7')
        == (IA('m3'), IA('d5'), IA('m7'))
    )

def interval_pattern_from_strings(
    *interval_analysis_strs: str
) -> IntervalPattern:
    interval_analyses = [
        interval_analysis_from_string(i) for i in interval_analysis_strs
    ]
    return tuple(interval_analyses)

IP = interval_pattern_from_strings # abbrev.

# Core functions
def test_analyze_chord():
    assert (
        analyze_chord(C('C', 'E', 'G'))
        == [{
            'bass': N('C'),
            'root': N('C'),
            'inversion': 0,
            'category': 'major',
            'symbol': 'C'
        }]
    )
    assert (
        analyze_chord(C('Eb', 'C', 'G'))
        == [{
            'bass': N('Eb'),
            'root': N('C'),
            'inversion': 1,
            'category': 'minor',
            'symbol': 'C-/Eb'
        }]
    )
    assert (
        analyze_chord(C('G#', 'E', 'C'))
        == [{
            'bass': N('G#'),
            'root': N('C'),
            'inversion': 2,
            'category': 'augmented',
            'symbol': 'C+/G#'
        }]
    )
    assert (
        analyze_chord(C('Db', 'Abb', 'Fb'))
        == [{
            'bass': N('Db'),
            'root': N('Db'),
            'inversion': 0,
            'category': 'diminished',
            'symbol': 'Db-b5'
        }]
    )
    assert (
        analyze_chord(C('C', 'G', 'A'))
        == [{
            'bass': N('C'),
            'root': N('A'),
            'inversion': 1,
            'category': 'minor_seventh',
            'symbol': 'A-7/C'
        }]
    )
    assert (
        analyze_chord(C('D', 'F#', 'C#', 'A'))
        == [{
            'bass': N('D'),
            'root': N('D'),
            'inversion': 0,
            'category': 'major_seventh',
            'symbol': 'Dmaj7'
        }]
    )
    assert (
        analyze_chord(C('F', 'B', 'D', 'G'))
        == [{
            'bass': N('F'),
            'root': N('G'),
            'inversion': 3,
            'category': 'dominant_seventh',
            'symbol': 'G7/F'
        }]
    )
    assert (
        analyze_chord(C('B', 'D', 'F', 'A'))
        == [{
            'bass': N('B'),
            'root': N('B'),
            'inversion': 0,
            'category': 'half_diminished',
            'symbol': 'B-7b5'
        }]
    )
    assert (
        analyze_chord(C('G#', 'B', 'D', 'F'))
        == [{
            'bass': N('G#'),
            'root': N('G#'),
            'inversion': 0,
            'category': 'diminished_seventh',
            'symbol': 'G#o'
        }]
    )
    assert (
        analyze_chord(C('C', 'F', 'G')) # multiple analysis
        == [{
            'bass': N('C'),
            'root': N('C'),
            'inversion': 0,
            'category': 'suspended_fourth',
            'symbol': 'Csus4'
            },
            {
            'bass': N('C'),
            'root': N('F'),
            'inversion': 2,
            'category': 'suspended_second',
            'symbol': 'Fsus2/C'
            },
        ]
    )
    assert not analyze_chord(C('C', 'D', 'E')) # unknown chord

def analyze_chord(chord: Chord) -> Sequence[ChordAnalysis]:
    analyses: List[ChordAnalysis] = []
    category_patterns = CHORD_CATEGORIES.keys()
    candidates = chord_arrangements(chord)
    for candidate in candidates:
        candidate_pattern = get_chord_pattern(candidate)
        if candidate_pattern in category_patterns:
            bass = get_bass(chord)
            root = get_bass(candidate)
            inversion = get_inversion(root, bass)
            category = get_category(candidate_pattern)
            symbol = build_chord_symbol(root, category, bass)
            analysis = {
                'bass': bass,
                'root': root,
                'inversion': inversion,
                'category': category,
                'symbol': symbol
            }
            analyses.append(analysis)
    return analyses

def test_chord_arrangements():
    assert (
        chord_arrangements(C('C', 'E'))
        == [C('C', 'E'), C('E', 'C')]
    )
    assert (
        chord_arrangements(C('C', 'E', 'G'))
        == [C('C', 'E', 'G'),
            C('C', 'G', 'E'),
            C('E', 'C', 'G'),
            C('E', 'G', 'C'),
            C('G', 'C', 'E'),
            C('G', 'E', 'C')]
    )

def chord_arrangements(chord: Chord) -> List[Chord]:
    return [Chord(notes) for notes in permutations(chord)]

def test_get_chord_pattern():
    assert get_chord_pattern(C('F', 'A')) == IP('M3')
    assert get_chord_pattern(C('C', 'E', 'G')) == IP('M3', 'P5')
    assert get_chord_pattern(C('Eb', 'B', 'F')) == IP('A5', 'M2')

def get_chord_pattern(chord: Chord) -> IntervalPattern:
    return analyze_intervals(intervals_from_bass(chord))

def test_analyze_intervals():
    assert analyze_intervals([I('C', 'E')]) == IP('M3')
    assert analyze_intervals([I('C', 'E'), I('C', 'G')]) == IP('M3', 'P5')
    assert (
        analyze_intervals([I('C', 'E'), I('C', 'G'), I('C', 'Bb')])
        == IP('M3', 'P5', 'm7')
    )

def analyze_intervals(intervals: Sequence[Interval]) -> IntervalPattern:
    return tuple(analyze_interval(interval) for interval in intervals)

def test_analyze_interval():
    assert analyze_interval(I('C', 'C')) == IA('P1')
    assert analyze_interval(I('C', 'D')) == IA('M2')
    assert analyze_interval(I('C', 'E')) == IA('M3')
    assert analyze_interval(I('C', 'F')) == IA('P4')
    assert analyze_interval(I('C', 'G')) == IA('P5')
    assert analyze_interval(I('C', 'A')) == IA('M6')
    assert analyze_interval(I('C', 'B')) == IA('M7')
    assert analyze_interval(I('D', 'G')) == IA('P4')
    assert analyze_interval(I('A#', 'B')) == IA('m2')
    assert analyze_interval(I('Gb', 'D#')) == IA('AA5')
    assert analyze_interval(I('D#', 'G')) == IA('d4')
    assert analyze_interval(I('C#', 'C')) is None
    assert analyze_interval(I('Cx', 'C')) is  None
    assert analyze_interval(I('C#', 'Dbb')) is None

def analyze_interval(interval: Interval) -> Optional[IntervalAnalysis]:
    interval_number = get_interval_number(interval)
    semitones = count_semitones(interval)
    interval_quality = get_interval_quality(interval_number, semitones)
    if not interval_quality:
        return None
    return IntervalAnalysis(interval_number, interval_quality)

def test_get_interval_number():
    assert get_interval_number(I('C', 'C')) == 1
    assert get_interval_number(I('C', 'F')) == 4
    assert get_interval_number(I('E', 'G')) == 3
    assert get_interval_number(I('A', 'D')) == 4

def get_interval_number(interval: Interval) -> IntervalNumber:
    low_letter = interval.low.letter
    high_letter = interval.high.letter
    interval_number = letter_distance(low_letter, high_letter) + 1
    return cast(IntervalNumber, interval_number)

def test_letter_distance():
    assert letter_distance('C', 'C') == 0
    assert letter_distance('C', 'F') == 3
    assert letter_distance('E', 'G') == 2
    assert letter_distance('A', 'D') == 3

def letter_distance(letter1: Letter, letter2: Letter) -> int:
    return (letter_number(letter2) - letter_number(letter1)) % 7

def test_letter_number():
    assert letter_number('C') == 0
    assert letter_number('D') == 1
    assert letter_number('E') == 2
    assert letter_number('F') == 3
    assert letter_number('G') == 4
    assert letter_number('A') == 5
    assert letter_number('B') == 6

def letter_number(letter: Letter) -> LetterNumber:
    number = (ord(letter) - ord('C')) % 7
    return cast(LetterNumber, number)

def test_count_semitones():
    assert count_semitones(I('C', 'C')) == 0
    assert count_semitones(I('C', 'D#')) == 3
    assert count_semitones(I('Bb', 'F')) == 7
    assert count_semitones(I('G', 'B')) == 4
    assert count_semitones(I('Ax', 'Gb')) == 7

def count_semitones(interval: Interval) -> int:
    pitchclass_low = get_pitchclass(interval.low)
    pitchclass_high = get_pitchclass(interval.high)
    return (pitchclass_high - pitchclass_low) % 12

def test_get_pitchclass():
    assert get_pitchclass(N('C')) == 0
    assert get_pitchclass(N('C#')) == 1
    assert get_pitchclass(N('D')) == 2
    assert get_pitchclass(N('D#')) == 3
    assert get_pitchclass(N('E')) == 4
    assert get_pitchclass(N('F')) == 5
    assert get_pitchclass(N('F#')) == 6
    assert get_pitchclass(N('G')) == 7
    assert get_pitchclass(N('G#')) == 8
    assert get_pitchclass(N('A')) == 9
    assert get_pitchclass(N('A#')) == 10
    assert get_pitchclass(N('B')) == 11
    assert get_pitchclass(N('Cb')) == 11
    assert get_pitchclass(N('B#')) == 0

def get_pitchclass(note: Note) -> Pitchclass:
    letter_num = letter_number(note.letter)
    offset = get_accidental_offset(note.accidental)
    if letter_num < 3:
        pitchclass = (letter_num * 2 + offset) % 12
    else:
        pitchclass = (letter_num * 2 - 1 + offset) % 12
    return cast(Pitchclass, pitchclass)

def test_get_accidental_offset():
    assert get_accidental_offset('double_flat') == -2
    assert get_accidental_offset('flat') == -1
    assert get_accidental_offset('natural') == 0
    assert get_accidental_offset('sharp') == 1
    assert get_accidental_offset('double_sharp') == 2

def get_accidental_offset(accidental: Accidental) -> int:
    return ACCIDENTAL_OFFSETS[accidental]

def test_get_interval_quality():
    assert get_interval_quality(3, 4) == 'major'
    assert get_interval_quality(2, 1) == 'minor'
    assert get_interval_quality(4, 6) == 'augmented'
    assert get_interval_quality(7, 9) == 'diminished'
    assert get_interval_quality(3, 6) == 'double_augmented'
    assert get_interval_quality(7, 8) == 'double_diminished'
    assert get_interval_quality(5, 7) == 'perfect'

def get_interval_quality(
    interval_number: IntervalNumber, semitones: int
) -> Optional[IntervalQuality]:
    try:
        quality = INTERVAL_QUALITIES[interval_number][semitones]
    except KeyError:
        return None
    return quality

def test_intervals_from_bass():
    assert intervals_from_bass(C('C', 'F')) == [I('C', 'F')]
    assert (
        intervals_from_bass(C('C', 'E', 'G'))
        == [I('C', 'E'), I('C', 'G')]
    )
    assert (
        intervals_from_bass(C('E', 'G', 'C'))
        == [I('E', 'G'), I('E', 'C')]
    )

def intervals_from_bass(chord: Chord) -> Sequence[Interval]:
    bass = get_bass(chord)
    notes_above_bass = chord[1:]
    return [Interval(bass, note) for note in notes_above_bass]

def test_get_bass():
    assert get_bass(C('C', 'E', 'G')) == N('C')
    assert get_bass(C('E', 'C', 'G')) == N('E')
    assert get_bass(C('G', 'E', 'C')) == N('G')

def get_bass(chord: Chord) -> Note:
    return chord[0]

def test_get_inversion():
    assert get_inversion(N('C'), N('C')) == 0
    assert get_inversion(N('C'), N('E')) == 1
    assert get_inversion(N('C'), N('G')) == 2
    assert get_inversion(N('C'), N('B')) == 3
    assert get_inversion(N('C'), N('D')) == 4
    assert get_inversion(N('C'), N('F')) == 5
    assert get_inversion(N('C'), N('A')) == 6

def get_inversion(root: Note, bass: Note) -> ChordInversion:
    def is_odd(num):
        return num % 2 != 0
    interval_number = get_interval_number(Interval(root, bass))
    if is_odd(interval_number):
        inversion = interval_number // 2
    else:
        inversion = (interval_number + 7) // 2
    return cast(ChordInversion, inversion)

def test_get_category():
    assert get_category(MAJOR) == 'major'
    assert get_category(DIMINISHED_SEVENTH) == 'diminished_seventh'

def get_category(interval_pattern: IntervalPattern) -> ChordCategory:
    return CHORD_CATEGORIES[interval_pattern]

def test_build_chord_symbol():
    assert build_chord_symbol(N('C'), 'major', N('C')) == 'C'
    assert build_chord_symbol(N('C'), 'minor', N('Eb')) == 'C-/Eb'
    assert (
        build_chord_symbol(N('E'), 'half_diminished', N('Bb'))
        == 'E-7b5/Bb'
    )

def build_chord_symbol(
    root: Note, category: ChordCategory, bass: Note
) -> str:
    root_symbol = note_to_string(root)
    category_symbol = get_category_symbol(category)
    bass_symbol = f'/{note_to_string(bass)}' if root != bass else ''
    return f'{root_symbol}{category_symbol}{bass_symbol}'

def test_note_to_string():
    assert note_to_string(N('C')) == 'C'
    assert note_to_string(N('Eb')) == 'Eb'
    assert note_to_string(N('Ax')) == 'Ax'

def note_to_string(note: Note) -> str:
    return f'{note.letter}{get_accidental_symbol(note.accidental)}'

def test_get_accidental_symbol():
    assert get_accidental_symbol('double_flat') == 'bb'
    assert get_accidental_symbol('flat') == 'b'
    assert get_accidental_symbol('natural') == ''
    assert get_accidental_symbol('sharp') == '#'
    assert get_accidental_symbol('double_sharp') == 'x'

def get_accidental_symbol(accidental: Accidental) -> str:
    return ACCIDENTAL_SYMBOLS[accidental]

def test_get_category_symbol():
    assert get_category_symbol('major') == ''
    assert get_category_symbol('minor') == '-'
    assert get_category_symbol('augmented') == '+'
    assert get_category_symbol('diminished') == '-b5'
    assert get_category_symbol('half_diminished') == '-7b5'
    assert get_category_symbol('dominant_seventh') == '7'
    assert get_category_symbol('major_seventh') == 'maj7'
    assert get_category_symbol('minor_seventh') == '-7'
    assert get_category_symbol('diminished_seventh') == 'o'
    assert get_category_symbol('suspended_fourth') == 'sus4'
    assert get_category_symbol('suspended_second') == 'sus2'

def get_category_symbol(category: ChordCategory) -> str:
    return CHORD_CATEGORY_SYMBOLS[category]

if __name__ == '__main__':
    # Real example of chord analysis
    SOR_35_9 = [
        C('A', 'C#', 'E'),
        C('G#', 'B', 'E'),
        C('F#', 'A', 'D'),
        C('E', 'A', 'C#'),
        C('D', 'A', 'F#'),
        C('C#', 'A', 'E'),
        C('B', 'G#', 'E', 'D'),
        C('A', 'C#'),
        C('E', 'G#', 'B'),
        C('A', 'C#'),
        C('B', 'A', 'D#'),
        C('C#', 'G#', 'E'),
        C('A', 'F#', 'C#', 'E'),
        C('B', 'F#', 'A', 'D#'),
        C('E', 'G#', 'B', 'E'),
        C('E', 'G#', 'B', 'E'),
        C('A', 'C#', 'E'),
        C('G#', 'B', 'E'),
        C('F#', 'A', 'D'),
        C('E', 'A', 'C#'),
        C('D', 'A', 'F#'),
        C('C#', 'A', 'E'),
        C('B', 'D', 'G#'),
        C('A', 'C#'),
        C('D', 'F#'),
        C('B', 'D', 'F#'),
        C('G#', 'D', 'E'),
        C('A', 'C#', 'E'),
        C('D', 'B'),
        C('E', 'G#', 'B'),
        C('A', 'E', 'C#'),
        C('E', 'G#', 'B'),
        C('E', 'G#', 'B'),
        C('E', 'A', 'C#'),
        C('E', 'C#'),
        C('E', 'B', 'D'),
        C('E', 'A', 'C#'),
        C('E', 'A', 'C#'),
        C('E', 'G#', 'B'),
        C('E', 'G#', 'B'),
        C('E', 'G#', 'B'),
        C('E', 'A', 'C#'),
        C('E', 'C#'),
        C('E', 'B', 'D'),
        C('E', 'A', 'C#'),
        C('E', 'G#', 'B'),
        C('E', 'G#', 'B'),
        C('A', 'C#'),
        C('B', 'A', 'D'),
        C('C#', 'A', 'E'),
        C('C#', 'A', 'E#'),
        C('D', 'A', 'F#'),
        C('C#', 'A#', 'E'),
        C('B', 'D#'),
        C('D#', 'A', 'F#'),
        C('E', 'G#'),
        C('E', 'G#', 'D'),
        C('E#', 'G#', 'C#'),
        C('F#', 'A', 'C#'),
        C('D', 'A', 'B'),
        C('D#', 'A', 'B'),
        C('E', 'G#', 'B'),
        C('A', 'C#', 'E'),
        C('G#', 'B', 'E'),
        C('F#', 'A', 'D'),
        C('E', 'A', 'C#'),
        C('D', 'A', 'F#'),
        C('C#', 'A', 'E'),
        C('B', 'G#', 'E', 'D'),
        C('A', 'C#'),
        C('E', 'G#', 'B'),
        C('A', 'C#'),
        C('B', 'F#', 'D'),
        C('E', 'G#', 'B'),
        C('A', 'C#'),
        C('D', 'B', 'E'),
        C('E', 'A', 'C#'),
        C('G#', 'B', 'E'),
        C('A', 'C#', 'E'),
        C('G#', 'B', 'E'),
        C('F#', 'A'),
        C('E', 'A', 'G#'),
        C('D', 'A', 'F#'),
        C('C#', 'A', 'E'),
        C('B', 'G#', 'D', 'E'),
        C('A', 'E', 'C#'),
        C('D#', 'A', 'B'),
        C('D', 'G#', 'F#'),
        C('C#', 'A', 'E'),
        C('D', 'B'),
        C('E', 'A', 'C#'),
        C('E', 'G#', 'B'),
        C('A', 'C#', 'E')
    ]

    def show_piece_analysis(piece: Sequence[Chord]) -> None:
        def show_chord(chord: Chord) -> str:
            notes_str = ', '.join([note_to_string(n) for n in chord])
            return f"Chord({notes_str})"
        def show_chord_symbol(analysis: Sequence[ChordAnalysis]) -> str:
            match analysis:
                case []:
                    symbol = '?'
                case [*ans]:
                    symbol = '|'.join([a['symbol'] for a in ans])
            return symbol
        chord_hdr = 'CHORD'
        analysis_hdr = 'ANALYSIS'
        print(f"{chord_hdr:<30}{analysis_hdr}")
        for chord in piece:
            analysis = analyze_chord(chord)
            chord_str = show_chord(chord)
            analysis_str = show_chord_symbol(analysis)
            print(f"{chord_str:<30}{analysis_str}")

    show_piece_analysis(SOR_35_9)
