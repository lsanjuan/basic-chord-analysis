#!/bin/sh

# Extract code blocks from a markdown file

MDFILE="$1"

sed -n '/^~~~/,/^~~~/ p' "$MDFILE" | sed '/^~~~/ d' | sed '0,/^$/{//d}' | cat -s 

